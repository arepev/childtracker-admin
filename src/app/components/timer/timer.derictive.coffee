angular.module 'childtrackerAdmin'
.directive 'timerEditor', ->
  TimerController = (moment, API, localStorageService) ->
    'ngInject'
    vm = this
    vm.hours = 0
    vm.minutes = 0
    t = localStorageService.get 'timer'
    vm.countDownTime = t || 0
    console.log 'TIMER IS', vm.countDownTime

    vm.timeUp = (type)->
      if type is 'hour'
        if vm.hours < 12
          vm.hours += 1
      if type is 'minutes'
        if vm.minutes < 55
          vm.minutes += 1

    vm.timeDown = (type)->
      if type is 'hour'
        if vm.hours > 0
          vm.hours -= 1
      if type is 'minutes'
        if vm.minutes > 0
          vm.minutes -= 1

    vm.startCountDownTimer = ->
      time = vm.hours * 60 * 60
      time = time + (vm.minutes * 60)
      curTime = new Date()
      msec = time * 1000
      time = msec + curTime.getTime()
      vm.countDownTime = time

      localStorageService.set('timer', vm.countDownTime)

      API.post('play/control', timeout: msec)

    vm.sendOff = ->
      vm.countDownTime = 0
      localStorageService.set('timer', vm.countDownTime)
      API.post('play/control', the_end: true)

    vm.stopTimer = ->
      vm.countDownTime = 0
      localStorageService.set('timer', vm.countDownTime)

      API.post('play/control', timeout: 0)

    return

  directive =
    restrict: 'E'
    templateUrl: 'app/components/timer/timer.html'
    controller: TimerController
    controllerAs: 'timer'
    bindToController: true

.filter 'timerOutput', ()->
  (time)->
    console.log '>>>>>', time
    if time < 10
      return '0' + time
    else
      return time

