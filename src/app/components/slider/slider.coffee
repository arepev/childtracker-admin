angular.module 'childtrackerAdmin'
  .directive 'slider', ->
    SliderController = (moment, $scope, localStorageService) ->
      'ngInject'
      vm = this
      curSlider = localStorageService.get 'slider'
      vm.priceSlider = {
        value: curSlider || 4
        options:
          floor: 0
          ceil: 23
      }

      $scope.$on "slideEnded", ->
        console.log 'YAhoo!', vm.priceSlider.value
        localStorageService.set 'slider', vm.priceSlider.value

      return

    directive =
      restrict: 'E'
      templateUrl: 'app/components/slider/slider.html'
      controller: SliderController
      controllerAs: 'slider'
      bindToController: true
