angular.module 'childtrackerAdmin'
.run ($log, $rootScope, Session, $state) ->
  'ngInject'
  $log.debug 'runBlock end'

  $rootScope.$on '$stateChangeStart', (event, toState, toParams) ->
    noAuth = toState.data? and toState.data.noAuth
    token = Session.get()

    if (not noAuth and not token)
      console.log 'noAuth access attempt'
      event.preventDefault()
      $state.go 'welcome'
