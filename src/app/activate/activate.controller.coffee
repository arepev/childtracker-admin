angular.module 'childtrackerAdmin'
.controller 'ActivateController', ($log, $stateParams, $state, Session) ->
  'ngInject'

  $log.info 'activate: token', $stateParams.token
  return if not $stateParams.token?
  token = angular.copy $stateParams.token
  Session.set(token)
  $state.go 'dashboard.statistics'


