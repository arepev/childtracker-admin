angular.module 'childtrackerAdmin'
.controller 'StatisticsController', ($log, $scope, API) ->
  'ngInject'
  randomNum = (max, min = 0) ->
    Math.floor(Math.random() * (max - min) + min)

  $scope.labels = [1..30]
  $scope.series = ['Series A']
  $scope.data = [
    [randomNum(2),randomNum(2),randomNum(5),randomNum(2), randomNum(8), randomNum(4), randomNum(3), randomNum(6), randomNum(4), randomNum(9), randomNum(3), randomNum(2), randomNum(3), randomNum(3), randomNum(2), randomNum(5),randomNum(1), randomNum(6), randomNum(6), randomNum(4), randomNum(1), randomNum(4), randomNum(3), randomNum(4)]
  ]

  init = ->
    $scope.stats = {}
    API.get('statistics').then (res) ->
      $scope.stats = res.data

  init()
