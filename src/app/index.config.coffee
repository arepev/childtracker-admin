angular.module 'childtrackerAdmin'
.config ($logProvider, $locationProvider, localStorageServiceProvider, $httpProvider) ->
  'ngInject'
  # Enable log
  $logProvider.debugEnabled true
  # $locationProvider.html5Mode
  #   enabled: true
  #   requireBase: false

  #CORS configuration
  $httpProvider.defaults.useXDomain = true
  delete $httpProvider.defaults.headers.common['X-Requested-With']

  localStorageServiceProvider.setPrefix('')
