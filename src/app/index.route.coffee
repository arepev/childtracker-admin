angular.module 'childtrackerAdmin'
.config ($stateProvider, $urlRouterProvider) ->
  'ngInject'

  $urlRouterProvider.otherwise '/welcome'

  $stateProvider
# entry point, saves token and uses it as session ID
  .state 'activate',
    url: '/activate/:token'
    templateUrl: 'app/activate/activate.html'
    controller: 'ActivateController as activate'
    data:
      noAuth: true

  .state 'welcome',
    url: '/welcome'
    templateUrl: 'app/welcome/welcome.html'
    controller: 'WelcomeController'
    data:
      noAuth: true

  .state 'dashboard',
    url: '/'
    templateUrl: 'app/dashboard/dashboard.html'
    controller: 'DashboardController'
    controllerAs: 'dashboard'

  .state 'dashboard.statistics',
    url: 'dashboard'
    templateUrl: 'app/statistics/statistics.html'
    controller: 'StatisticsController'

  .state 'playlists',
    url: '/playlist'
    templateUrl: 'app/playlist/playlists.html'
    controller: 'PlaylistsController as pls'

  .state 'playlist',
    url: '/playlist/:id'
    templateUrl: 'app/playlist/playlist.html'
    controller: 'PlaylistController as playlist'
