angular.module 'childtrackerAdmin'
.factory 'Session', ($log, localStorageService) ->
  'ngInject'
  tokenKey = 'token'
  new class Session
    constructor: ->
      @token = localStorageService.get tokenKey

    set: (token) ->
      $log.info 'setting new token', token
      localStorageService.set(tokenKey, token)
      @token = token

    get: ->
      @token

    remove: ->
      $log.info 'removing token', token
      localStorageService.remove tokenKey
      @token = null
