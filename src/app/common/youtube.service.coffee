angular.module 'childtrackerAdmin'
.factory 'Youtube', ($log, $http, Session, $q) ->
  devKey = 'AIzaSyAcCD29p-DjMSS5AWywEOny8Th13pJ2TgI'
  baseUrl = 'https://www.googleapis.com/youtube/v3/search?'

  new class Youtube
    qSearch: (input) ->
      qParam = 'q=' + input.split(' ').join('+')
      url = baseUrl + qParam + '&part=snippet&key=' + devKey
      $http.get(url)

# https://content.googleapis.com/youtube/v3/search?part=snippet&key=AIzaSyCFj15TpkchL4OUhLD1Q2zgxQnMb7v3XaM
