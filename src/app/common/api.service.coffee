angular.module 'childtrackerAdmin'
.factory 'API', ($log, $http, Session, $q) ->
  baseUrl = 'http://api.childtracker.co'
  new class API
    constructor: ->
      @v =
        id: 'zkxqRthhwIs'
        thumbnails:
          default:
            url: "https://i.ytimg.com/vi/zkxqRthhwIs/default.jpg"
          high:
            url: "https://i.ytimg.com/vi/zkxqRthhwIs/hqdefault.jpg"
          medium:
            url: "https://i.ytimg.com/vi/zkxqRthhwIs/mqdefault.jpg"
        title: 'Tom and Jerry Beach'
        description: "http://tomproducoes.com.br Download de Musicas: http://funkfodastico.net CURTI http://www.facebook.com/tomproducoesvideoclipes Tom Produções® - ©2015 ..."
        channel: "UCfwMtPbRx38cg7qoQJ0iUlQ"
        etag: "\"mPrpS7Nrk6Ggi_P7VJ8-KsEOiIw/mRCZ8HUZbJlMftHpfn5PuR4_5wU\""
      @video =
        data: @v
      @videos =
        data: [@v, @v, @v, @v, @v, @v, @v, @v, @v, @v, @v, @v, @v, @v, @v, @v, @v, @v, @v, @v, @v]
      @p =
        id: 'asdasdasd'
        name: 'name'
        active: true
      @plists =
        data: [@p, @p, @p, @p, @p, @p, @p, @p, @p]

    url: (slug) ->
      token = Session.get()
      if not token
        $log.error 'Attempted unauthorised API request'
        return
      baseUrl + '/' + token + '/' + slug

    get: (slug) ->
      $http.get @url(slug)
#      firstPart = slug.split('/')[0]
#      defer = $q.defer()
#      switch firstPart
#        when 'getcurrent' then defer.resolve(@video)
#        when 'lists' then defer.resolve(@videos)
#        when 'lists_x' then defer.resolve(@plists)
#      return defer.promise

    post: (slug, params) ->
      $http.post @url(slug), params

    put: (slug, params) ->
      $http.put @url(slug), params

    delete: (slug) ->
      $http.delete @url(slug)

    wipeDB: ->
      $http.get baseUrl + '/flushAll'
