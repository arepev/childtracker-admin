angular.module 'childtrackerAdmin'
.controller 'PlaylistsController', ($log, $scope, $stateParams, API, Youtube, lodash, $state) ->
  'ngInject'

  init = ->
    $scope.playlists = []

    API.get('lists').then (res) ->
      $log.info "lists", res.data
      $scope.playlists = res.data

  init()

  $scope.create = ->
    o =
      id: new Date().getTime()
      name: 'Новая подборка'
      active: true

    $log.info 'create', o
    $scope.playlists.push o

    API.post('lists', $scope.playlists).then (res) ->
      $state.go 'playlist', id: o.id

  $scope.delete = (i) ->
    $scope.playlists = lodash.reject $scope.playlists, (n) ->
      i.id is n.id
    $log.info 'left', $scope.playlists
    API.post('lists', $scope.playlists)

  $scope.toggle = (i) ->
    i.active = !i.active
    API.post('lists', $scope.playlists)
