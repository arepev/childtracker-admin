angular.module 'childtrackerAdmin'
.controller 'PlaylistController', ($log, $scope, $stateParams, API, Youtube, lodash) ->
  'ngInject'
  playlistId = null
  allPlaylists = []
  init = ->
    $scope.videos = {}
    # $scope.currentVideo = 'sMKoNBRZM1M'
    $scope.searchVideo = ''

    playlistId = $stateParams.id
    if not $stateParams.id
      $log.error 'Empty playlist id access'
      return

    API.get('lists').then (res) ->
      $log.info "lists", res.data
      allPlaylists = res.data
      $scope.playlist = lodash.find res.data, (n) ->
        n.id is parseInt(playlistId)
      $log.info 'list', $scope.playlist

    API.get('lists/' + playlistId).then (res) ->
      $log.info "lists/", res.data
      $scope.videos = res.data
      # $scope.currentVideo = res.data[0].id if res.data[0]?

  init()

  $scope.playerVars = {controls: 0, autoplay: 1}

  $scope.edit = false
  $scope.rename = ->
    $scope.edit = !$scope.edit

  $scope.saveTitle = ->
    $scope.edit = false
    # $scope.playlist
    API.post('lists', allPlaylists)

  $scope.search = ->
    if not $scope.searchRequest
      return

    Youtube.qSearch($scope.searchRequest).then (res) ->
      $scope.foundVideos = res.data.items

  $scope.preview = (id) ->
    $scope.currentVideo = id

  $scope.addToList = (i) ->
    res = lodash.find $scope.videos, (n) ->
      i.id.videoId is n.id
    if res
      $log.info 'already present', res
      return

    i.disabled = true

    v =
      id: i.id.videoId
      thumbnails: i.snippet.thumbnails
      title: i.snippet.title
      description: i.snippet.description
      channel: i.snippet.channelId
      etag: i.etag

    $scope.videos.push v

    if $scope.videos.length > 0
      $scope.playlist.thumbnail = $scope.videos[0].thumbnails.high.url
      $scope.saveTitle()


    $scope.preview(v.id)

    API.post('lists/' + playlistId, $scope.videos)

  $scope.deleteFromList = (i) ->
    $scope.videos = lodash.reject $scope.videos, (n) ->
      i.id is n.id
    $log.info 'left', $scope.videos
    API.post('lists/' + playlistId, $scope.videos)
